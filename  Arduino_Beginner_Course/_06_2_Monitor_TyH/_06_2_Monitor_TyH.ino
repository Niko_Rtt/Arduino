/********************************************************************************************/
/*  
 *Blink(Parpadeo)
 *
 *Enciende un LED por un segundo, luego lo apaga por un segundo, repetidamente.
*/
/********************************************************************************************/

/********************************************************************************************/
//Librerias
/********************************************************************************************/

#include "DHT.h"
#include <LiquidCrystal.h>

/********************************************************************************************/
//Definiciones
/********************************************************************************************/

//#define DHTTYPE DHT11     // DHT 11
#define DHTTYPE DHT22       // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21     // DHT 21 (AM2301)
#define DHT_PIN 9

#define RS_PIN 12    // Register Select 
#define EN_PIN 11    // Enable 
#define D4_PIN 5
#define D5_PIN 4
#define D6_PIN 3
#define D7_PIN 2

#define PRIMER_LINEA 0
#define SEGUNDA_LINEA 1

/********************************************************************************************/
//Variables Globales
/********************************************************************************************/

DHT dht(DHT_PIN, DHTTYPE);

LiquidCrystal lcd(RS_PIN, EN_PIN, D4_PIN, D5_PIN, D6_PIN, D7_PIN);

/********************************************************************************************/

// La función de "setup" es una configuración del microcontrolador que se ejecuta una vez
// cuando se presiona el boton reset o encendemos el arduino

void setup() {
  
    Serial.begin(9600);

    Serial.println("Monitor de Temperatura y Humedad");

    dht.begin();

    lcd.begin(16, 2);               // Inicializamos el numero de filas y columnas del LCD.

    lcd.clear();                    // Limpiamos toda la pantalla

    lcd.print("Hello, world!");     // Imprimimos el mensaje en el LCD.

    delay(2000);

    lcd.clear();                    // Limpiamos toda la pantalla

    lcd.print("Monitor de Tempe");     // Imprimimos el mensaje en el LCD.

    lcd.setCursor(0, SEGUNDA_LINEA);    // Ponemos el cursor en la segunda linea en la primer columna. 

    lcd.print("ratura y Humedad");     // Imprimimos el mensaje en el LCD.

    delay(2000);

    lcd.clear();                    // Limpiamos toda la pantalla

    lcd.print("TEMP:");     // Imprimimos el mensaje en el LCD.

    lcd.setCursor(14, PRIMER_LINEA);    // Ponemos el cursor en la segunda linea en la primer columna. 

    lcd.print("C");     // Imprimimos el mensaje en el LCD.

    lcd.setCursor(0, SEGUNDA_LINEA);    // Ponemos el cursor en la segunda linea en la primer columna. 

    lcd.print("HUMI:");     // Imprimimos el mensaje en el LCD.

    lcd.setCursor(14, SEGUNDA_LINEA);    // Ponemos el cursor en la segunda linea en la primer columna. 

    lcd.print("%");     // Imprimimos el mensaje en el LCD.
}

/********************************************************************************************/

// la función "loop" es un bucle que se repite una y otra vez para siempre

void loop() {
 
    float h = dht.readHumidity();           // La lectura de temperatura tarda alrededor de 250 ms.
    
    float t = dht.readTemperature();        // La lectura de temperatura tarda alrededor de 250 ms.

    if (isnan(h) || isnan(t)) {
        
        Serial.println("Falla la lectura del sensor DHT!");
    }

    else {

        Serial.print("Humedad: ");
        Serial.print(h);
        Serial.print(" %\t");
        Serial.print("Temperatura: ");
        Serial.print(t);
        Serial.println(" ºC ");

        lcd.setCursor(8, PRIMER_LINEA);    // Ponemos el cursor en la segunda linea en la primer columna. 
        lcd.print(t);         // Imprimimos el tiempo que paso en la segunda linea.
        lcd.setCursor(8, SEGUNDA_LINEA);    // Ponemos el cursor en la segunda linea en la primer columna. 
        lcd.print(h);         // Imprimimos el tiempo que paso en la segunda linea.
    }

    delay(1000);
}

/********************************************************************************************/