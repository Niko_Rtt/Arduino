/********************************************************************************************/
/*  
 *Teclado
 *
 *Realizamos la funcion para leer las teclas presionadas en un teclado matricial.
*/
/********************************************************************************************/
/********************************************************************************************/
//Definiciones
/********************************************************************************************/

#define LED_PIN     10

#define FILA_1	    9
#define FILA_2	    8
#define FILA_3	    7
#define FILA_4	    6

#define COLUMNA_1	5
#define COLUMNA_2	4
#define COLUMNA_3	3
#define COLUMNA_4	2

#define NO_TECLA    128
#define PERIODO_TECLAS_SEGUIDAS 300 //Esto es medio segundo entre detectar una tecla presionada y la misma mantenida.

/********************************************************************************************/
//Variables globales
/********************************************************************************************/

unsigned long Tiempo_Anterior = 0;
int Tecla_Presionada = NO_TECLA;

/********************************************************************************************/
//Declaracion de Funciones
/********************************************************************************************/

void Configurar_Teclado (void);
void Barrer_Teclado (void);
int Detectar_Tecla (void);
bool Antirrebote (int Tecla_Anterior);

/********************************************************************************************/

// La función de "setup" es una configuración del microcontrolador que se ejecuta una vez
// cuando se presiona el boton reset o encendemos el arduino

void setup() {

    Serial.begin(9600);
  
    Configurar_Teclado();           // Inicializamos todo el teclado matricial.
}

/********************************************************************************************/

// la función "loop" es un bucle que se repite una y otra vez para siempre

void loop() {

    Barrer_Teclado();           // Barremos el teclado para saber si presionaron una tecla

    delay(100);                 // esperamos por 100 milisegundos
}

/********************************************************************************************/
//Funciones Auxiliares
/********************************************************************************************/
/**
 * @brief Esta funcion configura todas las filas y columnas del teclado segun a que pin del
 *        Arduino correspondan, tanto salidas como entradas correspondientemente. 
 */

void Configurar_Teclado (void){

    // Seteo de filas como salidas digitales e inciadas en valor alto

    pinMode(FILA_1, OUTPUT);    // Inicializamos el pin digital FILA_1 como salida.

    pinMode(FILA_2, OUTPUT);    // Inicializamos el pin digital FILA_2 como salida.

    pinMode(FILA_3, OUTPUT);    // Inicializamos el pin digital FILA_3 como salida.

    pinMode(FILA_4, OUTPUT);    // Inicializamos el pin digital FILA_4 como salida.

    digitalWrite(FILA_1, HIGH); // encendemos el FILA_1 (HIGH es alto nivel de voltaje)

    digitalWrite(FILA_2, HIGH); // encendemos el FILA_2 (HIGH es alto nivel de voltaje)

    digitalWrite(FILA_3, HIGH); // encendemos el FILA_3 (HIGH es alto nivel de voltaje)

    digitalWrite(FILA_4, HIGH); // encendemos el FILA_4 (HIGH es alto nivel de voltaje)

    // Seteo de columnas como entrada con Pull-up, de esta manera se puede leer una tecla presionada como valor 0,
    // sino la entrada siempre va a estar en valor HIGH(alto nivel de voltaje) por la configuracion de pull-up.

    pinMode(COLUMNA_1, INPUT_PULLUP);   // Inicializamos el pin digital COLUMNA_1 como entrada.

    pinMode(COLUMNA_2, INPUT_PULLUP);   // Inicializamos el pin digital COLUMNA_2 como entrada.

    pinMode(COLUMNA_3, INPUT_PULLUP);   // Inicializamos el pin digital COLUMNA_3 como entrada.

    pinMode(COLUMNA_4, INPUT_PULLUP);   // Inicializamos el pin digital COLUMNA_4 como entrada.

    //Seteo el pin del led indicar de que se presiono una tecla.
    
    pinMode(LED_PIN, OUTPUT);       // Inicializamos el pin digital LED_PIN como salida.

    digitalWrite(LED_PIN, LOW);     // encendemos el LED_PIN (LOW es bajo nivel de voltaje)
}

/********************************************************************************************/
/**
 * @brief Esta tarea se encarga de preguntar si se detecto una tecla presionada, y encuestar
 *        por la misma tecla luego del tiempo de antirebote y prender un led para mostrar
 *        efectivamente que se presiono la tecla.
 * 
 * @return unsigned int Devuelve el valor de la tecla presionada.
 */

void Barrer_Teclado (void){

    int Tecla;

    Tecla = Detectar_Tecla();               // Barremos el teclado para saber si presionaron una tecla

    if (Tecla != NO_TECLA && Tecla != Tecla_Presionada){ // Detección de tecla pulsada y que no sea igual a la que se presiono la ultima vez hace medio segundo

        if (Antirrebote(Tecla) == true){    // Chequeamos si luego del tiempo de rebote sigue presionada la tecla
        
            digitalWrite(LED_PIN, HIGH);    // encendemos el LED (HIGH es alto nivel de voltaje)

            delay(50);                      // esperamos por 50 milisegundos

            digitalWrite(LED_PIN, LOW);     // apagamos el LED (LOW es bajo nivel de voltaje)

            Tecla_Presionada = Tecla;
            
            Tiempo_Anterior = millis();
            
            Serial.println(Tecla);
        }
    }

    if(millis() > (Tiempo_Anterior + PERIODO_TECLAS_SEGUIDAS)){

        Tecla_Presionada = NO_TECLA;
    }
}

/********************************************************************************************/
/**
 * @brief Esta funcion se fija en que fila y columna se esta presionando una tecla y devuelve
 *        el valor de la tecla presionada correspondiente.
 * 
 * @return char 
 */

int Detectar_Tecla (void){

    digitalWrite(FILA_1, LOW);                      // Poner en LOW la fila correspondiente
    digitalWrite(FILA_2, HIGH);                     // Poner en HIGH la fila correspondiente
    digitalWrite(FILA_3, HIGH);                     // Poner en HIGH la fila correspondiente
    digitalWrite(FILA_4, HIGH);                     // Poner en HIGH la fila correspondiente

    if (digitalRead(COLUMNA_1) == LOW) return 1;

    if (digitalRead(COLUMNA_2) == LOW) return 2;

    if (digitalRead(COLUMNA_3) == LOW) return 3;

    if (digitalRead(COLUMNA_4) == LOW) return 65;

    digitalWrite(FILA_1, HIGH);                     // Poner en HIGH la fila correspondiente
    digitalWrite(FILA_2, LOW);                      // Poner en LOW la fila correspondiente
    digitalWrite(FILA_3, HIGH);                     // Poner en HIGH la fila correspondiente
    digitalWrite(FILA_4, HIGH);                     // Poner en HIGH la fila correspondiente

    if (digitalRead(COLUMNA_1) == LOW) return 4;

    if (digitalRead(COLUMNA_2) == LOW) return 5;

    if (digitalRead(COLUMNA_3) == LOW) return 6;

    if (digitalRead(COLUMNA_4) == LOW) return 66;

    digitalWrite(FILA_1, HIGH);                     // Poner en LOW la fila correspondiente
    digitalWrite(FILA_2, HIGH);                     // Poner en HIGH la fila correspondiente
    digitalWrite(FILA_3, LOW);                      // Poner en HIGH la fila correspondiente
    digitalWrite(FILA_4, HIGH);                     // Poner en HIGH la fila correspondiente

    if (digitalRead(COLUMNA_1) == LOW) return 7;

    if (digitalRead(COLUMNA_2) == LOW) return 8;

    if (digitalRead(COLUMNA_3) == LOW) return 9;

    if (digitalRead(COLUMNA_4) == LOW) return 67;

    digitalWrite(FILA_1, HIGH);                     // Poner en LOW la fila correspondiente
    digitalWrite(FILA_2, HIGH);                     // Poner en HIGH la fila correspondiente
    digitalWrite(FILA_3, HIGH);                     // Poner en HIGH la fila correspondiente
    digitalWrite(FILA_4, LOW);                      // Poner en HIGH la fila correspondiente

    if (digitalRead(COLUMNA_1) == LOW) return 35;

    if (digitalRead(COLUMNA_2) == LOW) return 0;

    if (digitalRead(COLUMNA_3) == LOW) return 42;

    if (digitalRead(COLUMNA_4) == LOW) return 68;

    return NO_TECLA;                                // Si llegue a este punto es que no se presiono nada
                                                           
}

/********************************************************************************************/
/**
 * @brief Esta funcion espera que pase el tiempo de antirebote y luego se fija si la misma
 *        tecla que se paso como parametro sigue presionada.
 * 
 * @param Tecla_Anterior 
 * @return true 
 * @return false 
 */

bool Antirrebote (int Tecla_Anterior){

    int Tecla_Nueva = NO_TECLA;           // Variable Local, no visible para otras funciones

    delay(15);                                  // Es el tiempo que esperamos para no tomar el rebote de pulsacion(15ms)

    Tecla_Nueva = Detectar_Tecla();        // Encuesto el teclado otra vez para tomar la tecla presionada

    if(Tecla_Anterior == Tecla_Nueva){

        return true;
    }

    else return false;

}

/********************************************************************************************/