/********************************************************************************************/
/*  
 *Blink(Parpadeo)
 *
 *Enciende un LED por un segundo, luego lo apaga por un segundo, repetidamente.
*/
/********************************************************************************************/

// La función de "setup" es una configuración del microcontrolador que se ejecuta una vez
// cuando se presiona el boton reset o encendemos el arduino

void setup() {
  
    pinMode(LED_BUILTIN, OUTPUT); // Inicializamos el pin digital LED_BUILTIN como salida.
  
}

/********************************************************************************************/

// la función "loop" es un bucle que se repite una y otra vez para siempre

void loop() {
  
    digitalWrite(LED_BUILTIN, HIGH);   // encendemos el LED (HIGH es alto nivel de voltaje)
    
    delay(1000);                       // esperamos por 1 segundo
    
    digitalWrite(LED_BUILTIN, LOW);    // apagamos el LED (LOW es bajo nivel de voltaje)
    
    delay(1000);                       // esperamos por 1 segundo
}

/********************************************************************************************/