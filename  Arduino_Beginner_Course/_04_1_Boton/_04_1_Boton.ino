/********************************************************************************************/
/*  
 *Teclado
 *
 *Realizamos la funcion para leer las teclas presionadas en un teclado matricial.
*/
/********************************************************************************************/
/********************************************************************************************/
//Definiciones
/********************************************************************************************/

#define SI          1
#define NO          0
#define LED_PIN     8
#define BOTON       9     
#define PERIODO_BOTON_SEGUIDO 300 //Esto es medio segundo entre detectar una tecla presionada y la misma mantenida.

/********************************************************************************************/
//Variables globales
/********************************************************************************************/

unsigned long Tiempo_Anterior = 0;

int flag_Detectado = NO;

/********************************************************************************************/

// La función de "setup" es una configuración del microcontrolador que se ejecuta una vez
// cuando se presiona el boton reset o encendemos el arduino

void setup() {

    Serial.begin(9600);     // Inicializamos la comunicacion con la PC en 9600 baud.
  
    pinMode(BOTON, INPUT);  // Inicializamos el pin digital BOTON como entrada.

    pinMode(LED_PIN, OUTPUT); // Inicializamos el pin digital LED_PIN como salida.
}

/********************************************************************************************/

// la función "loop" es un bucle que se repite una y otra vez para siempre

void loop() {

    if (digitalRead(BOTON) == LOW){                 // Pulsacion detectada

        delay(25);                                  // Es el tiempo que esperamos para no tomar el rebote de pulsacion(15ms)

        if (digitalRead(BOTON) == LOW && flag_Detectado == NO){     // Pulsacion detectada y flag desactivado, osea antes no estaba apretado.

            flag_Detectado = SI;                    // Activamos el flag para mostrar que es una nueva deteccion del boton
            
            digitalWrite(LED_PIN, HIGH);            // encendemos el LED (HIGH es alto nivel de voltaje)

            delay(50);                             // esperamos por 100 milisegundos

            digitalWrite(LED_PIN, LOW);             // apagamos el LED (LOW es bajo nivel de voltaje)

            Serial.println("Boton presionado.");    // Enviamos a la PC el estado.

            Tiempo_Anterior = millis();             // Cargamos el tiempo actual en la variable global creada.
        }

        if (millis() > (Tiempo_Anterior + PERIODO_BOTON_SEGUIDO)){  // Si se cumple que ya paso el tiempo para volver a chequear el estado del boton

            if (digitalRead(BOTON) == LOW){                         // Pulsacion detectada

                Serial.println("El Boton sigue presionado.");       // Enviamos a la PC el estado.

                Tiempo_Anterior = millis();                         // Cargamos el tiempo actual en la variable global creada.
            }

            else flag_Detectado = NO;                               // Desactivamos el flag para mostrar que es una nueva deteccion del boton
        }
    }

    else{
        
        flag_Detectado = NO;    // Desactivamos el flag para mostrar que es una nueva deteccion del boton
    }

    delay(20);                  // esperamos por 20 milisegundos
}

/********************************************************************************************/