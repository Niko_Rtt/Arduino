/********************************************************************************************/
/*  
 *Blink(Parpadeo)
 *
 *Enciende un LED por un segundo, luego lo apaga por un segundo, repetidamente.
*/
/********************************************************************************************/

/********************************************************************************************/
//Definiciones
/********************************************************************************************/

#define LED_PIN 9

/********************************************************************************************/

// La función de "setup" es una configuración del microcontrolador que se ejecuta una vez
// cuando se presiona el boton reset o encendemos el arduino

void setup() {
  
    Serial.begin(9600);     // Inicializamos la comunicacion con la PC en 9600 baud.
  
}

/********************************************************************************************/

// la función "loop" es un bucle que se repite una y otra vez para siempre

void loop() {

    long AnalogValue = 0;                           // Creamos la variable donde alojaremos el valor del ADC, que nos devuelve un valor
                                                    // que varia entre 0 y 1023

    int BrightnessValue = 0;                        // Creamos la variable con punto flotante que guardara el valor decimal de medicion

    AnalogValue = analogRead(A0);                   // Cargamos el valor leido por el ADC que varia entre 0-1023

    BrightnessValue = (AnalogValue * 255) / 1023;   // Convertimos el valor discreto en analogico

    Serial.print(AnalogValue);                      // Enviamos el valor que leimos del pin analogico

    Serial.print(" -> ");

    Serial.println(BrightnessValue);                // Enviamos el valor que cargaremos al PWM a la PC

    analogWrite(LED_PIN, BrightnessValue);          // Cargamos el nuevo valor de PWM en el pin de conexion del led
    
    delay(100);                                     // esperamos por 1 segundo
}

/********************************************************************************************/