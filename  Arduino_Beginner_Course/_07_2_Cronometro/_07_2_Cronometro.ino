/********************************************************************************************/
/*  
* Cronometro
*
* Funciona igual que un cronometro de manoa, con dos botones de inicio, parada y reinicio.
*/
/********************************************************************************************/

/********************************************************************************************/
//Librerias
/********************************************************************************************/

#include "TM1637.h"     // Libreria que debe ser instala en el IDE Arduino.
#include <TimerOne.h>

/********************************************************************************************/
//Definiciones
/********************************************************************************************/

#define SI             1
#define NO             0
#define ON             1
#define OFF            0
#define NO_BOTON       2
#define PAUSA          3

#define LED_PIN        10
#define CLK            3
#define DIO            2
#define BOTON_SET      4
#define BOTON_RESET    5
#define PERIODO_BOTON_SEGUIDO 300 //Esto es 300ms entre detectar una tecla presionada y la misma mantenida.

/********************************************************************************************/
//Variables Globales
/********************************************************************************************/

TM1637 Displays(CLK, DIO);      //set up the 4-Digit Display.

int8_t TimeDisp[] = {0,0,0,0};    // Numero a mostrar en los displays

unsigned long Tiempo_Anterior = 0;

int Boton_Presionado = NO_BOTON, Boton_Ultimo = NO_BOTON;

int Flag_ActualizarDisplays = 0, Flag_Accion = OFF;

int Minutos = 0, Segundos = 0, Microsegundos = 0;

/********************************************************************************************/
//Declaracion de Funciones
/********************************************************************************************/

void Configurar_Botones (void);
int Barrer_Botones (void);
int Detectar_Boton (void);
bool Antirrebote (int Boton_Anterior);
void TimingISR (void);
void Actualizando_Displays (void);

/********************************************************************************************/

// La función de "setup" es una configuración del microcontrolador que se ejecuta una vez
// cuando se presiona el boton reset o encendemos el arduino

void setup() {
  
    Configurar_Botones();               // Inicializamos los pines digitales de los botones como entrada.

    Displays.init();                    // Funcion con la que se inicializa el shield de displays 7 segmentos

    Displays.set(0);                    // Funcion que setea el brillo de los displays 7 segmentos, 0 es el minimo y 7 maximo

    Displays.point(POINT_OFF);          // Apaga dos puntos, POINT_ON para encenderlo y POINT_OFF para apagarlos

    Displays.display(TimeDisp);         // Mostramos en el display el valor de TimeDisp.    

    Timer1.initialize(100000);          // Inicializamos el timer 1 para que nos interrumpa cada 100ms
    
    Timer1.attachInterrupt(TimingISR);  //declare the interrupt serve routine:TimingISR  
}

/********************************************************************************************/

// la función "loop" es un bucle que se repite una y otra vez para siempre

void loop() {

    int Boton = 0;

    Boton = Barrer_Botones();  // Barremos el teclado para saber si presionaron un Boton

    if (Boton == BOTON_SET && Boton_Ultimo == NO_BOTON){

        Flag_Accion = ON;

        Boton_Ultimo = BOTON_SET;
    }

    else if (Boton == BOTON_SET &&  Boton_Ultimo == BOTON_SET){

        if(Flag_Accion == ON) Flag_Accion = PAUSA; 
        
        else if(Flag_Accion == PAUSA) Flag_Accion = ON; 
    }

    else if (Boton == BOTON_RESET){

        Flag_Accion = OFF;

        Boton_Ultimo = NO_BOTON;

        Microsegundos = 0;

        Segundos = 0;

        Minutos = 0;

        Flag_ActualizarDisplays = SI;
    }
    
    if(Flag_ActualizarDisplays == SI){

        Flag_ActualizarDisplays = NO;

        Actualizando_Displays();
        
        Displays.display(TimeDisp); // Mostramos en el displat el valor de TimeDisp.
    }

    delay(1);
}

/********************************************************************************************/

/********************************************************************************************/
//Funciones Auxiliares
/********************************************************************************************/
/**
 * @brief Esta funcion configura todas las filas y columnas del teclado segun a que pin del
 *        Arduino correspondan, tanto salidas como entradas correspondientemente. 
 */

void Configurar_Botones (void){

    pinMode(BOTON_RESET, INPUT_PULLUP);   // Inicializamos el pin digital COLUMNA_3 como entrada.

    pinMode(BOTON_SET, INPUT_PULLUP);   // Inicializamos el pin digital COLUMNA_4 como entrada.

    //Seteo el pin del led indicar de que se presiono una tecla.
    
    pinMode(LED_PIN, OUTPUT);       // Inicializamos el pin digital LED_PIN como salida.

    digitalWrite(LED_PIN, LOW);     // encendemos el LED_PIN (LOW es bajo nivel de voltaje)
}

/********************************************************************************************/
/**
 * @brief Esta tarea se encarga de preguntar si se detecto una tecla presionada, y encuestar
 *        por la misma tecla luego del tiempo de antirebote y prender un led para mostrar
 *        efectivamente que se presiono la tecla.
 * 
 * @return unsigned int Devuelve el valor de la tecla presionada.
 */

int Barrer_Botones (void){

    int Boton = NO_BOTON;

    Boton = Detectar_Boton();               // Barremos todos los botones para saber si presionaron algun Boton

    if (Boton != NO_BOTON && Boton != Boton_Presionado){ // Detección de Boton pulsada y que no sea igual a la que se presiono la ultima vez hace medio segundo

        if (Antirrebote(Boton) == true){    // Chequeamos si luego del tiempo de rebote sigue presionada la Boton
        
            digitalWrite(LED_PIN, HIGH);    // encendemos el LED (HIGH es alto nivel de voltaje)

            delay(20);                      // esperamos por 20 milisegundos

            digitalWrite(LED_PIN, LOW);     // apagamos el LED (LOW es bajo nivel de voltaje)

            Boton_Presionado = Boton;
            
            Tiempo_Anterior = millis();

            return Boton;         
        }
    }

    if(millis() > (Tiempo_Anterior + PERIODO_BOTON_SEGUIDO)){

        Boton_Presionado = NO_BOTON;
    }

    return NO_BOTON;
}

/********************************************************************************************/
/**
 * @brief Esta funcion se fija en que fila y columna se esta presionando una tecla y devuelve
 *        el valor de la tecla presionada correspondiente.
 * 
 * @return char 
 */

int Detectar_Boton (void){

    int Boton_detec;

    if (digitalRead(BOTON_RESET) == LOW) return BOTON_RESET;

    if (digitalRead(BOTON_SET) == LOW) return BOTON_SET;

    return NO_BOTON;
}

/********************************************************************************************/
/**
 * @brief Esta funcion espera que pase el tiempo de antirebote y luego se fija si la misma
 *        tecla que se paso como parametro sigue presionada.
 * 
 * @param Tecla_Anterior 
 * @return true 
 * @return false 
 */

bool Antirrebote (int Boton_Anterior){

    int Boton_Nuevo = NO_BOTON;           // Variable Local, no visible para otras funciones

    delay(15);                            // Es el tiempo que esperamos para no tomar el rebote de pulsacion(15ms)

    Boton_Nuevo = Detectar_Boton();       // Encuesto el Boton otra vez para tomar el Boton como presionado

    if(Boton_Anterior == Boton_Nuevo){

        return true;
    }

    else return false;

}

/********************************************************************************************/
/**
 * @brief Esta funcion se ejecutara en cada interrupcion del sistema para actualizar los
 *        valores de los tiempos a mostrar en el display si la visualizacion esta activada.
 *  
 */

void TimingISR (void){

    if (Flag_Accion == ON){

        Microsegundos ++;

        if(Microsegundos == 10){

            Microsegundos = 0;

            Segundos ++;

            if(Segundos == 60){
            
                Segundos = 0;

                Minutos ++;

                if(Minutos == 10){

                    Microsegundos = 0;

                    Segundos = 0;

                    Minutos = 0;
                }
            }   
        }
        
        Flag_ActualizarDisplays = SI;
    }
}

/********************************************************************************************/
/**
 * @brief Con esta funcion calcularemos el valor a mostrar en los displays 7 segmentos cada
 *        vez que haya que actualizar los mismos.
 * 
 */

void Actualizando_Displays (void){

    TimeDisp[3] = Microsegundos;
    
    TimeDisp[2] = Segundos % 10;
    
    TimeDisp[1] = Segundos / 10;
    
    TimeDisp[0] = Minutos;
}

/********************************************************************************************/