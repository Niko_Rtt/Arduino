/********************************************************************************************/
/*  
 *Semaforo Doble
 *
 *Utilizamos la secuencia del semaforo realizada con anterioridad y la replicamos para realizar
 *una secuencia de semaforos doble como en la vida real
*/
/********************************************************************************************/

/********************************************************************************************/
//Definiciones
/********************************************************************************************/

#define LED_ROJO_1      7
#define LED_AMARILLO_1  6
#define LED_VERDE_1     5

#define LED_ROJO_2      10
#define LED_AMARILLO_2  9
#define LED_VERDE_2     8

#define Tiempo_VyR      10000
#define Tiempo_Amarillo 2000

/********************************************************************************************/

// La función de "setup" es una configuración del microcontrolador que se ejecuta una vez
// cuando se presiona el boton reset o encendemos el arduino

void setup() {
  
    pinMode(LED_ROJO_1, OUTPUT);        // Inicializamos el pin digital LED_ROJO_1 como salida.

    pinMode(LED_AMARILLO_1, OUTPUT);    // Inicializamos el pin digital LED_AMARILLO_1 como salida.

    pinMode(LED_VERDE_1, OUTPUT);       // Inicializamos el pin digital LED_VERDE_1 como salida.

    pinMode(LED_ROJO_2, OUTPUT);        // Inicializamos el pin digital LED_ROJO_2 como salida.

    pinMode(LED_AMARILLO_2, OUTPUT);    // Inicializamos el pin digital LED_AMARILLO_2 como salida.

    pinMode(LED_VERDE_2, OUTPUT);       // Inicializamos el pin digital LED_VERDE_2 como salida.

    digitalWrite(LED_ROJO_1, LOW);      // encendemos el LED_ROJO_1 (LOW es bajo nivel de voltaje)

    digitalWrite(LED_AMARILLO_1, LOW);  // encendemos el LED_AMARILLO_1 (LOW es bajo nivel de voltaje)

    digitalWrite(LED_VERDE_1, LOW);     // encendemos el LED_VERDE_1 (LOW es bajo nivel de voltaje)

    digitalWrite(LED_ROJO_2, LOW);      // encendemos el LED_ROJO_2 (LOW es bajo nivel de voltaje)

    digitalWrite(LED_AMARILLO_2, LOW);  // encendemos el LED_AMARILLO_2 (LOW es bajo nivel de voltaje)

    digitalWrite(LED_VERDE_2, LOW);     // encendemos el LED_VERDE_2 (LOW es bajo nivel de voltaje)
  
}

/********************************************************************************************/

// la función "loop" es un bucle que se repite una y otra vez para siempre

void loop() {
  
    Secuencia_Doble_Semaforo();
}

/********************************************************************************************/
//Funciones Auxiliares
/********************************************************************************************/

void Secuencia_Doble_Semaforo (void) {

    //Semaforo 1 en verde

    digitalWrite(LED_VERDE_1, HIGH);    // encendemos el LED_VERDE (HIGH es alto nivel de voltaje)

    digitalWrite(LED_ROJO_2, HIGH);     // encendemos el LED_VERDE (HIGH es alto nivel de voltaje)
    
    delay(Tiempo_VyR);                  // esperamos por un tiempo determinado
    
    digitalWrite(LED_VERDE_1, LOW);     // apagamos el LED (LOW es bajo nivel de voltaje)


    //Semaforo 1 en amarillo
    
    digitalWrite(LED_AMARILLO_1, HIGH); // encendemos el LED_VERDE (HIGH es alto nivel de voltaje)
    
    delay(Tiempo_Amarillo);             // esperamos por un tiempo determinado
    
    digitalWrite(LED_AMARILLO_1, LOW);  // apagamos el LED (LOW es bajo nivel de voltaje)

    digitalWrite(LED_ROJO_2, LOW);      // encendemos el LED_VERDE (HIGH es alto nivel de voltaje)


    //Semaforo 2 en verde
        
    digitalWrite(LED_ROJO_1, HIGH);     // encendemos el LED_VERDE (HIGH es alto nivel de voltaje)

    digitalWrite(LED_VERDE_2, HIGH);    // encendemos el LED_VERDE (HIGH es alto nivel de voltaje)
    
    delay(Tiempo_VyR);                  // esperamos por un tiempo determinado

    digitalWrite(LED_VERDE_2, LOW);     // encendemos el LED_VERDE (HIGH es alto nivel de voltaje)


    //Semaforo 2 en amarillo
    
    digitalWrite(LED_AMARILLO_2, HIGH); // encendemos el LED_VERDE (HIGH es alto nivel de voltaje)
    
    delay(Tiempo_Amarillo);             // esperamos por un tiempo determinado
    
    digitalWrite(LED_AMARILLO_2, LOW);  // apagamos el LED (LOW es bajo nivel de voltaje)

    digitalWrite(LED_ROJO_1, LOW);      // encendemos el LED_VERDE (HIGH es alto nivel de voltaje)

}

/********************************************************************************************/
