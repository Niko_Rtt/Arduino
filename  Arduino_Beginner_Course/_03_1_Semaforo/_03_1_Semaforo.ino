/********************************************************************************************/
/*  
 *Semaforo
 *
 *Realizamos la secuencia de un semaforo de manera externa con todo lo aprendido hasta el
 *momento.
*/
/********************************************************************************************/

/********************************************************************************************/
//Definiciones
/********************************************************************************************/

#define LED_ROJO        7
#define LED_AMARILLO    6
#define LED_VERDE       5

#define Tiempo_Verde    10000
#define Tiempo_Amarillo 2000
#define Tiempo_Rojo     10000

/********************************************************************************************/

// La función de "setup" es una configuración del microcontrolador que se ejecuta una vez
// cuando se presiona el boton reset o encendemos el arduino

void setup() {
  
    pinMode(LED_ROJO, OUTPUT);          // Inicializamos el pin digital LED_ROJO como salida.

    pinMode(LED_AMARILLO, OUTPUT);      // Inicializamos el pin digital LED_AMARILLO como salida.

    pinMode(LED_VERDE, OUTPUT);         // Inicializamos el pin digital LED_VERDE como salida.

    digitalWrite(LED_ROJO, LOW);        // encendemos el LED_ROJO (LOW es bajo nivel de voltaje)

    digitalWrite(LED_AMARILLO, LOW);    // encendemos el LED_AMARILLO (LOW es bajo nivel de voltaje)

    digitalWrite(LED_VERDE, LOW);       // encendemos el LED_VERDE (LOW es bajo nivel de voltaje)
  
}

/********************************************************************************************/

// la función "loop" es un bucle que se repite una y otra vez para siempre

void loop() {
  
    digitalWrite(LED_VERDE, HIGH);      // encendemos el LED_VERDE (HIGH es alto nivel de voltaje)
    
    delay(Tiempo_Verde);                // esperamos por un tiempo determinado
    
    digitalWrite(LED_VERDE, LOW);       // apagamos el LED_VERDE (LOW es bajo nivel de voltaje)

    
    digitalWrite(LED_AMARILLO, HIGH);   // encendemos el LED_AMARILLO (HIGH es alto nivel de voltaje)
    
    delay(Tiempo_Amarillo);             // esperamos por un tiempo determinado
    
    digitalWrite(LED_AMARILLO, LOW);    // apagamos el LED_AMARILLO (LOW es bajo nivel de voltaje)

        
    digitalWrite(LED_ROJO, HIGH);       // encendemos el LED_ROJO (HIGH es alto nivel de voltaje)
    
    delay(Tiempo_Rojo);                 // esperamos por un tiempo determinado
    
    digitalWrite(LED_ROJO, LOW);        // apagamos el LED_ROJO (LOW es bajo nivel de voltaje)
}

/********************************************************************************************/