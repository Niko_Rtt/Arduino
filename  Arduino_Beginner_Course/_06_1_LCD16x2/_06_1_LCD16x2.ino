/********************************************************************************************/
/*  
 *Blink(Parpadeo)
 *
 *Enciende un LED por un segundo, luego lo apaga por un segundo, repetidamente.
*/
/********************************************************************************************/

/********************************************************************************************/
//Librerias
/********************************************************************************************/

#include <LiquidCrystal.h>

/********************************************************************************************/
//Definiciones
/********************************************************************************************/

#define RS_PIN 12    // Register Select 
#define EN_PIN 11    // Enable 
#define D4_PIN 5
#define D5_PIN 4
#define D6_PIN 3
#define D7_PIN 2

#define PRIMER_LINEA 0
#define SEGUNDA_LINEA 1

/********************************************************************************************/
//Variables Globales
/********************************************************************************************/

LiquidCrystal lcd(RS_PIN, EN_PIN, D4_PIN, D5_PIN, D6_PIN, D7_PIN);

/********************************************************************************************/

// La función de "setup" es una configuración del microcontrolador que se ejecuta una vez
// cuando se presiona el boton reset o encendemos el arduino

void setup() {

    lcd.begin(16, 2);               // Inicializamos el numero de filas y columnas del LCD.

    lcd.clear();                    // Limpiamos toda la pantalla

    lcd.print("hello, world!");     // Imprimimos el mensaje en el LCD.

    delay(2000);
  
}

/********************************************************************************************/

// la función "loop" es un bucle que se repite una y otra vez para siempre

void loop() {
  
    lcd.setCursor(0, SEGUNDA_LINEA);    // Ponemos el cursor en la segunda linea en la primer columna. 

    lcd.print(millis() / 1000);         // Imprimimos el tiempo que paso en la segunda linea.

    delay(500);
}

/********************************************************************************************/