/********************************************************************************************/
/*  
* Display 7 Segmentos
*
* Enciende los displays, muestra un numero y titila continuamente.
*/
/********************************************************************************************/

/********************************************************************************************/
//Librerias
/********************************************************************************************/

#include "TM1637.h"     // Libreria que debe ser instala en el IDE Arduino.

/********************************************************************************************/
//Definiciones
/********************************************************************************************/

#define CLK 3
#define DIO 2

/********************************************************************************************/
//Variables Globales
/********************************************************************************************/

TM1637 Displays(CLK, DIO);      //set up the 4-Digit Display.

int8_t Digits[] = {1,2,3,4};    // Numero a mostrar en los displays

/********************************************************************************************/

// La función de "setup" es una configuración del microcontrolador que se ejecuta una vez
// cuando se presiona el boton reset o encendemos el arduino

void setup() {

    Displays.init();            // Funcion con la que se inicializa el shield de displays 7 segmentos

    Displays.set(0);            // Funcion que setea el brillo de los displays 7 segmentos, 0 es el minimo y 7 maximo

    Displays.point(POINT_OFF);  // Apaga dos puntos, POINT_ON para encenderlo y POINT_OFF para apagarlos
}

/********************************************************************************************/

// la función "loop" es un bucle que se repite una y otra vez para siempre

void loop() {
    
    delay(1000);                // esperamos por 1 segundo*/

    Displays.display(Digits);   // Mostramos en el displat el valor de Digits.

    delay(1000);                // esperamos por 1 segundo*/

    Displays.clearDisplay();    // Limpia y apaga la visualizacion del display

}

/********************************************************************************************/