/********************************************************************************************/
/*  
 *Blink(Parpadeo)
 *
 *Enciende un LED por un segundo, luego lo apaga por un segundo, repetidamente.
*/
/********************************************************************************************/

/********************************************************************************************/
//Definiciones
/********************************************************************************************/

#define LED_PIN 9

/********************************************************************************************/

// La función de "setup" es una configuración del microcontrolador que se ejecuta una vez
// cuando se presiona el boton reset o encendemos el arduino

void setup() {
  
    Serial.begin(9600);     // Inicializamos la comunicacion con la PC en 9600 baud.
  
}

/********************************************************************************************/

// la función "loop" es un bucle que se repite una y otra vez para siempre

void loop() {

    int BrightnessValue = 0;                            // Creamos la variable con punto flotante que guardara el valor decimal de medicion

    int ReceiveValue = 0;                               // Creamos la variable con la cual recibiremos el valor enviado de la PC

    if (Serial.available()) {                           // Si hay un valor enviado por la PC entonces lo cargamos

        ReceiveValue = Serial.parseInt();               // Cargamos el valor enviado por la PC, valor comprendido entre 0 y 100%

        BrightnessValue = (ReceiveValue * 255) / 100;   // Convertimos el valor discreto en analogico
        
        analogWrite(LED_PIN, BrightnessValue);

        Serial.print(ReceiveValue);                     // Enviamos el valor que leimos del pin analogico

        Serial.print("% -> ");

        Serial.println(BrightnessValue);                // Enviamos el valor que cargaremos al PWM a la PC
    }
}

/********************************************************************************************/