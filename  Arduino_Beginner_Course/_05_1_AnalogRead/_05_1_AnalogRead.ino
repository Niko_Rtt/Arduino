/********************************************************************************************/
/*  
 * Lectura Analogica de Entrada
 *
 * Leemos la entrada analogica del ARduino a la cual conectamos un resistor variable y 3,3v.
*/
/********************************************************************************************/

/********************************************************************************************/

// La función de "setup" es una configuración del microcontrolador que se ejecuta una vez
// cuando se presiona el boton reset o encendemos el arduino

void setup() {
  
    Serial.begin(9600);     // Inicializamos la comunicacion con la PC en 9600 baud.
  
}

/********************************************************************************************/

// la función "loop" es un bucle que se repite una y otra vez para siempre

void loop() {

    int AnalogValue = 0;                        // Creamos la variable donde alojaremos el valor del ADC, que nos devuelve un valor
                                                // que varia entre 0 y 1023

    float Voltage = 0;                          // Creamos la variable con punto flotante que guardara el valor decimal de medicion

    AnalogValue = analogRead(A0);               // Cargamos el valor leido por el ADC que varia entre 0-1023

    Voltage = AnalogValue * (5.0 / 1023.0);     // Convertimos el valor discreto en analogico

    Serial.println(Voltage);                    // Enviamos el valor que leimos a la PC
    
    delay(300);                                 // esperamos por 0.3 segundo
}

/********************************************************************************************/