/********************************************************************************************/
/*  
 *Blink Externo (Parpadeo)
 *
 *Enciende un LED conectado por medio de una resistencia a uno de los pines digitales de salida 
 *por un segundo, luego lo apaga por un segundo, repetidamente.
*/
/********************************************************************************************/

/********************************************************************************************/
//Definiciones
/********************************************************************************************/

#define LED_PIN 7

/********************************************************************************************/

// La función de "setup" es una configuración del microcontrolador que se ejecuta una vez
// cuando se presiona el boton reset o encendemos el arduino

void setup() {
  
    pinMode(LED_PIN, OUTPUT); // Inicializamos el pin digital LED_PIN como salida.
  
}

/********************************************************************************************/

// la función "loop" es un bucle que se repite una y otra vez para siempre

void loop() {
  
    digitalWrite(LED_PIN, HIGH);   // encendemos el LED (HIGH es alto nivel de voltaje)
    
    delay(1000);                       // esperamos por 1 segundo
    
    digitalWrite(LED_PIN, LOW);    // apagamos el LED (LOW es bajo nivel de voltaje)
    
    delay(1000);                       // esperamos por 1 segundo
}

/********************************************************************************************/
